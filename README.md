python script to run every 15 minutes in cron, logs into the Frontier D2200D DSL modem and accesses the adsl stat page, checks the 2nd to last 15 minute interval for reported Loss of Signal (LOS) and sends a tweet with TxCRC and SEF and a hashtag for your carrier
<br/>

Typical crontab entry:<br/>
*/15 * * * * DISPLAY=:0.0 /home/chuck/src/d2200dADSLStat/checkLOS.py<br/>
<br/>

Typical tweet:<br/>
LOS! DateTime: 09-30-2018 17:23:36, TxCRC: 168250, SEF: 21 #FrontierCommunications #FrontierCorp
