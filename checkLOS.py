#!/usr/bin/env python
# script to get modem adsl statistics for charting, tweeting, etc

import sys
from time import sleep
from selenium import webdriver
from BeautifulSoup import BeautifulSoup as bs
from selenium.webdriver.common.keys import Keys

# your app credentials from https://apps.twitter.com/
consumer_key        = 'ABCDEFGHIJKLMNOPQRSTU'
consumer_secret     = 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMN'
access_token        = '123456789-ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMN'
access_token_secret = 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNAB'

try:
   browser = webdriver.Firefox()
except:
   print("Remember to export DISPLAY=:0.0")

browser.get('http://192.168.1.1/htmlV/adslstat.asp')
alert = browser.switch_to_alert()
alert.send_keys('admin'+Keys.TAB+'admin')
alert.accept()

sleep(2)
innerHTML = browser.execute_script("return document.body.innerHTML")
page = bs(innerHTML)
browser.close()

# extract table of interest
table = page('table')[0]('table')[2]('table')[2]('table')[1]

# check next to last row so it will have reconnected by the time we try to tweet a report
row=len(table('tr'))-3
if ( int(table('tr')[row]('td')[5].text) > 0):
      message = "LOS! DateTime: " + table('tr')[row]('td')[0].text[0:10] + " " + table('tr')[row]('td')[0].text[10:18] + ", TxCRC: " + table('tr')[row]('td')[1].text + ", SEF: " + table('tr')[row]('td')[6].text + "  #FrontierCommunications #FrontierCorp"
      twitter = Twython(consumer_key, consumer_secret, access_token, access_token_secret)
      twitter.update_status(status=message)
      print("Tweeted: {}".format(message))


